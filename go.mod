module gitlab.com/oraxpool/orax-message

go 1.13

require (
	github.com/google/flatbuffers v1.11.0
	github.com/stretchr/testify v1.4.0
)
