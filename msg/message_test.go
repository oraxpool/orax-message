package msg

import (
	"crypto/rand"

	flatbuffers "github.com/google/flatbuffers/go"
	"gitlab.com/oraxpool/orax-message/msg/fbs"

	"testing"

	"github.com/stretchr/testify/require"
)

func TestStartMiningMessageSerialization(t *testing.T) {
	require := require.New(t)

	builder := flatbuffers.NewBuilder(1024)

	oprHash := make([]byte, 32)
	rand.Read(oprHash)

	startMining := NewStartMiningMessage(builder, oprHash)
	unmarshalled, err := UnmarshalMessage(startMining)

	require.NoError(err)
	typed := unmarshalled.(*fbs.StartMiningMessage)
	require.Equal(oprHash, typed.OprHashBytes())
}

func TestSubmissionWindowClosingMessageSerialization(t *testing.T) {
	require := require.New(t)

	builder := flatbuffers.NewBuilder(1024)

	deadline := uint8(5)
	windowClosing := NewSubmissionWindowClosingMessage(builder, deadline)
	unmarshalled, err := UnmarshalMessage(windowClosing)

	require.NoError(err)
	typed := unmarshalled.(*fbs.SubmissionWindowClosingMessage)
	require.Equal(deadline, typed.Deadline())
}

func TestSetTargetMessageSerialization(t *testing.T) {
	require := require.New(t)

	builder := flatbuffers.NewBuilder(1024)

	target := uint64(187875943543)
	setTarget := NewSetTargetMessage(builder, target)
	unmarshalled, err := UnmarshalMessage(setTarget)

	require.NoError(err)
	typed := unmarshalled.(*fbs.SetTargetMessage)
	require.Equal(target, typed.Target())
}

func TestSubmitMessageSerialization(t *testing.T) {
	require := require.New(t)

	builder := flatbuffers.NewBuilder(1024)

	nonces := make([][]byte, 20)
	for i := range nonces {
		nonce := make([]byte, 32)
		rand.Read(nonce)
		nonces[i] = nonce
	}

	submit := NewSubmitMessage(builder, nonces)
	unmarshalled, err := UnmarshalMessage(submit)

	require.NoError(err)
	typed := unmarshalled.(*fbs.SubmitMessage)

	for i, nonce := range nonces {
		n := new(fbs.Nonce)
		typed.Nonces(n, i)
		require.Equal(nonce, n.BytesBytes())

	}
}
