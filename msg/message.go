package msg

import (
	"errors"

	"gitlab.com/oraxpool/orax-message/msg/fbs"
)

type MessageType = uint8

const (
	// V0
	MineSignal      MessageType = iota
	SubmitSignal    MessageType = iota
	MinerSubmission MessageType = iota
	// V1 using FlatBuffers
	StartMining             MessageType = iota
	SubmissionWindowClosing MessageType = iota
	SetTarget               MessageType = iota
	Submit                  MessageType = iota
)

func UnmarshalMessage(bytes []byte) (interface{}, error) {
	if len(bytes) <= 1 {
		return nil, errors.New("Message too short")
	}

	payload := bytes[1:]
	switch bytes[0] {
	case Submit:
		return fbs.GetRootAsSubmitMessage(payload, 0), nil
	case StartMining:
		return fbs.GetRootAsStartMiningMessage(payload, 0), nil
	case SubmissionWindowClosing:
		return fbs.GetRootAsSubmissionWindowClosingMessage(payload, 0), nil
	case SetTarget:
		return fbs.GetRootAsSetTargetMessage(payload, 0), nil
	case MineSignal:
		return unmarshalMineSignalMessage(payload)
	case SubmitSignal:
		return unmarshalSubmitSignalMessage(payload)
	case MinerSubmission:
		return unmarshalMinerSubmissionMessage(payload)
	default:
		return nil, errors.New("Unknown message type")
	}
}
