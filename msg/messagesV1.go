package msg

import (
	flatbuffers "github.com/google/flatbuffers/go"
	"gitlab.com/oraxpool/orax-message/msg/fbs"
)

func NewStartMiningMessage(builder *flatbuffers.Builder, oprHash []byte) []byte {
	builder.Reset()

	oprh := builder.CreateByteVector(oprHash)
	fbs.StartMiningMessageStart(builder)
	fbs.StartMiningMessageAddOprHash(builder, oprh)
	builder.Finish(fbs.StartMiningMessageEnd(builder))
	builder.PrependByte(StartMining)
	return builder.FinishedBytes()
}

func NewSubmissionWindowClosingMessage(builder *flatbuffers.Builder, deadline uint8) []byte {
	builder.Reset()

	fbs.SubmissionWindowClosingMessageStart(builder)
	fbs.SubmissionWindowClosingMessageAddDeadline(builder, deadline)
	builder.Finish(fbs.SubmissionWindowClosingMessageEnd(builder))
	builder.PrependByte(SubmissionWindowClosing)
	return builder.FinishedBytes()
}

func NewSetTargetMessage(builder *flatbuffers.Builder, target uint64) []byte {
	builder.Reset()

	fbs.SetTargetMessageStart(builder)
	fbs.SetTargetMessageAddTarget(builder, target)
	builder.Finish(fbs.SetTargetMessageEnd(builder))
	builder.PrependByte(SetTarget)
	return builder.FinishedBytes()
}

func NewSubmitMessage(builder *flatbuffers.Builder, nonces [][]byte) []byte {
	builder.Reset()

	nonceCount := len(nonces)

	// Create FBS Nonces
	noncesFbs := make([]flatbuffers.UOffsetT, nonceCount)
	for i, nonce := range nonces {
		byteVector := builder.CreateByteVector(nonce)
		fbs.NonceStart(builder)
		fbs.NonceAddBytes(builder, byteVector)
		noncesFbs[i] = fbs.NonceEnd(builder)
	}

	// Create vector of FBS Nonces
	fbs.SubmitMessageStartNoncesVector(builder, nonceCount)
	for i := nonceCount - 1; i >= 0; i-- {
		builder.PrependUOffsetT(noncesFbs[i])
	}
	noncesVector := builder.EndVector(nonceCount)

	// Create message
	fbs.SubmitMessageStart(builder)
	fbs.SubmitMessageAddNonces(builder, noncesVector)
	builder.Finish(fbs.SubmitMessageEnd(builder))
	builder.PrependByte(Submit)

	return builder.FinishedBytes()
}
