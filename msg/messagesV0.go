package msg

import (
	"encoding/json"
)

type MineSignalMessage struct {
	MaxNonces int    `json:"maxNonces"`
	OprHash   []byte `json:"oprHash"`
}

func NewMineSignalMessage(oprHash []byte, maxNonces int) *MineSignalMessage {
	m := new(MineSignalMessage)
	m.OprHash = oprHash
	m.MaxNonces = maxNonces
	return m
}

type SubmitSignalMessage struct {
	WindowDurationSec int `json:"oprHash"`
}

type MinerSubmissionMessage struct {
	OprHash  []byte  `json:"oprHash"`
	Nonces   []Nonce `json:"nonces"`
	Duration int64   `json:"duration"`
	OpCount  int64   `json:"opCount"`
}

type Nonce struct {
	Nonce      []byte `json:"nonce"`
	Difficulty uint64 `json:"diff"`
}

func (msm *MineSignalMessage) Marshal() ([]byte, error) {
	return marshal(MineSignal, msm)
}

func (ssm *SubmitSignalMessage) Marshal() ([]byte, error) {
	return marshal(SubmitSignal, ssm)
}

func (msm *MinerSubmissionMessage) Marshal() ([]byte, error) {
	return marshal(MinerSubmission, msm)
}

func marshal(t MessageType, i interface{}) ([]byte, error) {
	bytes := []byte{t}

	j, err := json.Marshal(i)
	if err != nil {
		return []byte{}, err
	}
	bytes = append(bytes, j...)

	return bytes, nil
}

////////////////////////////////////////
// Unmarshall
////////////////////////////////////////

func unmarshalMineSignalMessage(bytes []byte) (*MineSignalMessage, error) {
	var msm MineSignalMessage
	err := json.Unmarshal(bytes, &msm)
	return &msm, err
}

func unmarshalSubmitSignalMessage(bytes []byte) (*SubmitSignalMessage, error) {
	var ssm SubmitSignalMessage
	err := json.Unmarshal(bytes, &ssm)
	return &ssm, err
}

func unmarshalMinerSubmissionMessage(bytes []byte) (*MinerSubmissionMessage, error) {
	var msm MinerSubmissionMessage
	err := json.Unmarshal(bytes, &msm)
	return &msm, err
}
