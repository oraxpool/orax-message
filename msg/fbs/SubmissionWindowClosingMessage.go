// Code generated by the FlatBuffers compiler. DO NOT EDIT.

package fbs

import (
	flatbuffers "github.com/google/flatbuffers/go"
)

type SubmissionWindowClosingMessage struct {
	_tab flatbuffers.Table
}

func GetRootAsSubmissionWindowClosingMessage(buf []byte, offset flatbuffers.UOffsetT) *SubmissionWindowClosingMessage {
	n := flatbuffers.GetUOffsetT(buf[offset:])
	x := &SubmissionWindowClosingMessage{}
	x.Init(buf, n+offset)
	return x
}

func (rcv *SubmissionWindowClosingMessage) Init(buf []byte, i flatbuffers.UOffsetT) {
	rcv._tab.Bytes = buf
	rcv._tab.Pos = i
}

func (rcv *SubmissionWindowClosingMessage) Table() flatbuffers.Table {
	return rcv._tab
}

func (rcv *SubmissionWindowClosingMessage) Deadline() byte {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(4))
	if o != 0 {
		return rcv._tab.GetByte(o + rcv._tab.Pos)
	}
	return 0
}

func (rcv *SubmissionWindowClosingMessage) MutateDeadline(n byte) bool {
	return rcv._tab.MutateByteSlot(4, n)
}

func SubmissionWindowClosingMessageStart(builder *flatbuffers.Builder) {
	builder.StartObject(1)
}
func SubmissionWindowClosingMessageAddDeadline(builder *flatbuffers.Builder, deadline byte) {
	builder.PrependByteSlot(0, deadline, 0)
}
func SubmissionWindowClosingMessageEnd(builder *flatbuffers.Builder) flatbuffers.UOffsetT {
	return builder.EndObject()
}
